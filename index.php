<?php
/**
 * It is a front controller
 * All request come to here
 */

require __DIR__ . '/src/autoload.php';

$uri = basename($_SERVER['REQUEST_URI']);

$controller = new \WebApp\Controller\ProductController();

switch($uri)
{
    /**
     * Product List
     */
    case 'junior_test':
    {
        $controller->renderListForm();
    }
    case 'index.php?list':
    {
        $controller->renderListForm();
    }
    break;
    /**
     * Product Add
     */
    case 'index.php?new':
    {
        $controller->renderAddingForm();
    }
    break;
    /**
     * Error 404
     */
    default:
    {
        $controller = new \WebApp\Controller\NotFoundController();
        $controller->renderNotFound();
    } 
    break;
}