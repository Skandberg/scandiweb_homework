<?php

namespace WebApp\Services;

/**
 * Represents a connection to a database server.
 */
class Database
{
    /**
     * PDO inputs
     * 
     * @var string
     */
    private $host = 'localhost';
    private $db_name = 'junior_test';
    private $db_username = 'root';
    private $db_password = '';

    private $pdo = null;
    private $stmt = null;
    
    /**
     *  Connect to the DB when Database class initialized.
     * 
     *  @return void
     */
    function __construct()
    {
        try
        {
            // connect
            $this->pdo = new \PDO(
                "mysql:host=$this->host;dbname=$this->db_name;",
                $this->db_username,
                $this->db_password);
        }
        catch (Exception $ex)
        {
            die($ex->getMessage());
        }
    }
    
    /**
     *  Clear PDO when Database class destroyed.
     * 
     *  @return void
     */
    function __destruct()
    {
        $this->stmt = null;
        $this->pdo = null;
    }

    /**
     *  "Insert", "Delete" or "Update" query.
     * 
     *  @return void
     */
    function query($sql)
    {
        try 
        {
            $this->stmt = $this->pdo->prepare($sql);
            $this->stmt->execute();
        }
        catch (Exception $ex)
        {
            die($ex->getMessage());
        }
    }

    /**
     *  Query to get an array of rows from a table.
     * 
     *  @return array
     */
    function queryFetchAll($sql): array
    {
        try 
        {
            $this->stmt = $this->pdo->prepare($sql);
            $this->stmt->execute();
            $arr = $this->stmt->fetchAll();
        }
        catch (Exception $ex)
        {
            die($ex->getMessage());
        }

        $this->stmt = null;
        return $arr;
    }
}