<?php

namespace WebApp\Services;

use WebApp\Model\Product;
use WebApp\Services\Database;

/**
 * ProductRepository fetches products as product classes from DB.
 * All queries are written here and they are send to Database class.
 */
class ProductRepository
{
    private $db;

    /**
     *  @return void
     */
    function __construct()
    {
        $this->db = new Database();
    }

    /**
     * 1. Get rows from DB;
     * 2. Put rows into an array[] of product classes.
     * 
     * @return array of products.
     */
    public function fetchProducts(): array
    {
        $dbProducts = $this->getProductsFromDatabase();
        $productArray = [];

        foreach ($dbProducts as $p) {

            $productModel = new Product();
            $productModel->setSku($p['sku']);
            $productModel->setName($p['name']);
            $productModel->setPrice($p['price']);
            $productModel->setValue($p['value']);
            $productModel->setAttributeValue($p['attribute_value']);
            $productModel->setAttributeName($p['attribute_name']);

            $productArray[] = $productModel;
        }

        return $productArray;
    }

    /**
     * Request to insert product into DB.
     * 
     * @return void
     */
    public function addProduct($sku, $name, $price, $attribute_id, $value)
    {
        $sql = 
        "INSERT INTO products (sku, name, price, attribute_id, value) 
        VALUES ('$sku', '$name', $price, $attribute_id, '$value');";

        $this->db->query($sql);
    }

    /**
     * Request to delete product from DB by sku number.
     * 
     * @return void
     */
    public function deleteProduct($sku)
    {
        $sql = "DELETE FROM products WHERE sku='$sku';";
        $this->db->query($sql);
    }

    /**
     * Request to delete array of products from DB by sku numbers.
     * 
     * @return void
     */
    public function deleteProductArray($skuArray)
    {
        foreach ($skuArray as $sku)
        {
            $this->deleteProduct($sku);
        }
    }

    /**
     * Get products from DB.
     * 
     * Merge "products" table and "attributes" table into one table 
     * and get rows from merged table.
     * 
     * @return array of rows.
     */
    private function getProductsFromDatabase(): array
    {
        $sql =
            "SELECT * FROM products
            INNER JOIN attributes
            ON products.attribute_id=attributes.attribute_id;";

        $arr = $this->db->queryFetchAll($sql);
        return $arr;
    }
}