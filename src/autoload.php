<?php

include_once "psr4_autoloader.php";

// instantiate the loader
$loader = new \Example\Psr4AutoloaderClass;

// register the autoloader
$loader->register();

// register the base directories for the namespace prefix
$loader->addNamespace('WebApp\Controller', 'src/Controller');
$loader->addNamespace('WebApp\Model', 'src/Model');
$loader->addNamespace('WebApp\Services', 'src/Services');
$loader->addNamespace('WebApp\View', 'src/View');