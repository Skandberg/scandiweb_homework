<?php

namespace WebApp\Controller;

class NotFoundController
{
    /**
     * Not found action
     *
     * @return void
     */
    public function renderNotFound()
    {
        require_once __DIR__ . '/../View/not-found.php';
    }
}