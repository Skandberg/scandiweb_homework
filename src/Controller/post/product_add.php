<?php

if (isset($_POST['product-type'])) 
{
    // get product type, like 1, 2, 3..
    $type = $_POST['product-type'];

    if (isset($_POST['sku']) and isset($_POST['name']) and isset($_POST['price']))
    {
        // get value from form
        $value = postValue($type);

        if ($value != null)
        {
            $productRepository = new \WebApp\Services\ProductRepository();
            // add new product into DB
            $productRepository->addProduct($_POST['sku'], $_POST['name'], $_POST['price'], $type, $value);
        }
    }
}

/**
 * $_POST value depending on switched product type.
 * 
 * @return string
 */
function postValue($type)
{
    switch ($type)
    {
        // DVD-Disk size
        case 1:
        {
            if (isset($_POST['size']))
            {
                return $_POST['size'];
            }
        }       
        break;
        // Book weight
        case 2:
        {
            if (isset($_POST['weight']))
            {
                return $_POST['weight'];
            }
        }       
        break;
        // Furniture dimentions HxWxL
        case 3:
        {
            if (isset($_POST['height']) and isset($_POST['width']) and isset($_POST['length']))
            {
                return $_POST['height'].'x'.$_POST['width'].'x'.$_POST['length'];
            }
        }       
        break;
        default: return null;
    }
}