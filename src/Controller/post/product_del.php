<?php

/**
 * Each value from checkbox(View/product_list page) contains sku number.
 */
if(isset($_POST['checkboxes']))
{
    // initialize ProductRepository class
    $productRepository = new \WebApp\Services\ProductRepository();
    // delete products using checkboxes[] array of sku numbers
    $productRepository->deleteProductArray($_POST['checkboxes']);
}