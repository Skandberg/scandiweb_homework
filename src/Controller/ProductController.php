<?php

namespace WebApp\Controller;

use WebApp\Services\ProductRepository;

class ProductController
{
    /**
     * Product list form
     *
     * @return void
     */
    public function renderListForm()
    {
        // POST mass delete action in DB
        include_once __DIR__.'/post/product_del.php';

        // request for product array from DB table
        $productRepository = new ProductRepository();
        $products = $productRepository->fetchProducts();

        // page "Product List"
        // $products variable uses in product_list.php
        require_once __DIR__ . '/../View/product_list.php';
    }

    /**
     * Product adding form
     *
     * @return void
     */
    public function renderAddingForm()
    {
        // POST add new product to database
        include_once __DIR__.'/post/product_add.php';
        
        // page "Product Add"
        require_once __DIR__ . '/../View/product_add.php';
    }
}