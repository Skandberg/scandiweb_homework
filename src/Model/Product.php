<?php

namespace WebApp\Model;

class Product
{
    /**
     * Sku
     *
     * @var string
     */
    private $sku;

    /**
     * Name
     *
     * @var string
     */
    private $name;

    /**
     * Price
     *
     * @var float
     */
    private $price;

    /**
     * Value
     *
     * Example: any number or HxWxL format
     * 
     * @var string
     */
    private $value;

     /**
     * Attribute value
     *
     * Example: MB(megabyte), GB(gigabyte), Kg..
     * 
     * @var string
     */
    private $attributeValue;

     /**
     * Attribute name
     *
     * Example: size, weight, dimensions..
     * 
     * @var string
     */
    private $attributeName;

    /**
     * Get sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set sku
     *
     * @param string $sku
     */
    public function setSku(string $sku)
    {
        $this->sku = $sku;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set price
     *
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set value
     *
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

     /**
     * Get attribute value
     *
     * Example: MB(megabyte), GB(gigabyte), Kg..
     * 
     * @return string
     */
    public function getAttributeValue()
    {
        return $this->attributeValue;
    }

    /**
     * Set attribute value
     *
     * @param string $attributeValue
     */
    public function setAttributeValue(string $attributeValue)
    {
        $this->attributeValue = $attributeValue;
    }

    /**
     * Get attribute name
     *
     * Example: size, weight, dimensions..
     * 
     * @return string
     */
    public function getAttributeName()
    {
        return $this->attributeName;
    }

    /**
     * Set attribute name
     *
     * @param string $attributeName
     */
    public function setAttributeName(string $attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * Get value description
     * 
     * Example: "Size" + "700" + "MB" = "Size 700 MB".
     *
     * @return string
     */
    public function getValueDetail()
    {
        return $this->attributeName. ' '. $this->value. ' '. $this->attributeValue;
    }
}