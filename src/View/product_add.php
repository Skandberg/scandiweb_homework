<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Product Add</title>
    <link rel="stylesheet" href="css/top_nav.css">
    <link rel="stylesheet" href="css/product_add.css">
    <script src="js/type_switcher.js"></script>
</head>

<body>
    <div class="topnav">
        <a href="index.php?list">Product List</a>
        <a class="active" href="index.php?new">Product Add</a>
        <button class="button_green" type="submit" form="product-add-form-id">Save</button>
    </div>

    <!-- "Product Add" form -->
    <form action="index.php?new" id="product-add-form-id" method="post">
        SKU:<br>
        <input required type="text" name="sku"><br>
        Name:<br>
        <input required type="text" name="name"><br>
        Price:<br>
        <input required type="number" step="0.01" min="0" name="price"><br><br>

        <!-- Product selection -->
        <select id="product-types-id" name="product-type" onchange="switchType(this.selectedIndex, 'subform-div');">
            <option value='1'>​DVD-disc</option>
            <option value='2'>Book</option>
            <option value='3'>Furniture</option>
        </select>

        <!-- DVD-disk subform -->
        <div class="subform-div">
            <br>Size:<br>
            <input name="size" type="number" min="0"><br>
            <p>Please provide weight in MB format</p>
        </div>

        <!-- Book subform -->
        <div class="subform-div">
            <br>Weight:<br>
            <input name="weight" type="number" min="0"><br>
            <p>Please provide weight in KG format</p>
        </div>

        <!-- Furniture subform -->
        <div class="subform-div">
            <br>Height:<br>
            <input name="height" type="number" min="0">
            <br>Width:<br>
            <input name="width" type="number" min="0">
            <br>Length:<br>
            <input name="length" type="number" min="0"><br>
            <p>Please provide dimensions in HxWxL format</p>
        </div>
    </form>
    <script>switchType(0, 'subform-div');</script>
</body>

</html>