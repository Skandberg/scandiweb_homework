<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Product List</title>
    <link rel="stylesheet" href="css/top_nav.css">
    <link rel="stylesheet" href="css/product_list.css">
</head>

<body>
    <div class="topnav">
        <a class="active" href="index.php?list">Product List</a>
        <a href="index.php?new">Product Add</a>
        <button class="button_red" type="submit" form="product-list-form-id">Mass delete action</button>
    </div>

    <!-- "Product List" form -->
    <form action="index.php?list" id="product-list-form-id" method="post">
        <?php
        foreach ($products as $product):
        ?>
        <div class="card_column">
            <div class="card">
                <!-- checkbox -->
                <div class="card_checkbox">
                    <input type="checkbox" name="checkboxes[]" value=<?php echo $product->getSku()?>>
                </div>
 
                <!-- SKU -->
                <h4><?php echo $product->getSku();?></h4>

                <!-- Name -->
                <p><?php echo $product->getName()?></p>

                <!-- Price -->
                <p><?php echo $product->getPrice().'$'?></p>

                <!-- Value -->
                <p><?php echo $product->getValueDetail()?></p>
            </div>
        </div>
        <?php
        endforeach; // next iteration
        ?>
    </form>
</body>